package ch.swissbytes.todo;

import ch.swissbytes.todo.views.ToDoListFreeMarkerEngine;
import spark.ModelAndView;
import spark.TemplateEngine;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.SparkBase.stop;

public class ToDoListMain {
    public static void main(String[] args) {
        EntityManager entityManager = new EntityManager();
        new ToDoListMain().startup(entityManager);
    }

    public void startup(EntityManager entityManager) {
        //EntityManager entityManager = new EntityManager();
        ToDoListFreeMarkerEngine templateEngine = new ToDoListFreeMarkerEngine();

        setupRoutes(entityManager, templateEngine);

        initialiseStartData(entityManager);
    }

    private void initialiseStartData(EntityManager entityManager) {
        entityManager.addNewItem("Item 1", "Description 1");
        entityManager.addNewItem("Item 2", "Description 2");
    }

    private void setupRoutes(EntityManager entityManager, ToDoListFreeMarkerEngine templateEngine) {
        completeItem(entityManager, templateEngine);
        setupItemsRoute(entityManager, templateEngine);
        setupItemsNewRoute(entityManager, templateEngine);
        setupItemsAdd(entityManager, templateEngine);
    }

    private void setupItemsRoute(EntityManager entityManager, TemplateEngine templateEngine) {
        get("/items", (request, response) -> {
            Map<String, Object> attributes = new HashMap<String, Object>();
            attributes.put("todoitems", entityManager.getItems());
            attributes.put("completeitems", entityManager.getCompletedItems());

            return new ModelAndView(attributes, "todolist.html");
        }, templateEngine);
    }

    private void setupItemsNewRoute(EntityManager entityManager, TemplateEngine templateEngine) {
        get("/items/new", (request, response) -> {
            Map<String, Object> attributes = new HashMap<String, Object>();
            return new ModelAndView(attributes, "new.html");
        }, templateEngine);
    }

    private void setupItemsAdd(EntityManager entityManager, TemplateEngine templateEngine) {
        post("/items/new", (request, response) -> {

            String title = request.queryParams("title");
            String description = request.queryParams("description");

            entityManager.addNewItem(title, description);

            Map<String, Object> attributes = new HashMap<String, Object>();
            attributes.put("success", true);
            return new ModelAndView(attributes, "new.html");
        }, templateEngine);
    }

    private void completeItem(EntityManager entityManager, TemplateEngine templateEngine) {
        get("/items", (request, response) -> {

            if (request.queryParams("id")!=null) {
                int id = Integer.parseInt(request.queryParams("id"));
                entityManager.completeItem(id);
            }

            Map<String, Object> attributes = new HashMap<String, Object>();
            attributes.put("todoitems", entityManager.getItems());
            attributes.put("completeitems", entityManager.getCompletedItems());

            return new ModelAndView(attributes, "todolist.html");
        }, templateEngine);
    }

    public void shutdown() {
        stop();
    }
}
