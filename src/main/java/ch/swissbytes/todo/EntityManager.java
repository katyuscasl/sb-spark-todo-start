package ch.swissbytes.todo;

import ch.swissbytes.todo.model.ToDoItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EntityManager {
    private List<ToDoItem> items;
    private List<ToDoItem> completedItems;

    public EntityManager() {
        items = new ArrayList<>();
        completedItems = new ArrayList<>();
    }

    public List<ToDoItem> getItems() {
        return Collections.unmodifiableList(items);
    }

    public List<ToDoItem> getCompletedItems() {
        return Collections.unmodifiableList(completedItems);
    }

    private int getNextId() {
        int id = 0;
        for (ToDoItem item : items) {
            if (id < item.getId()) {
                id = item.getId();
            }
        }

        return id + 1;
    }

    public ToDoItem addNewItem(String title, String description) {
        ToDoItem item = new ToDoItem(getNextId(), title, description);
        items.add(item);
        return item;
    }

    public ToDoItem completeItem(int id) {
        ToDoItem item = items.get(items.indexOf(new ToDoItem(id,null,null)));
        completedItems.add(item);
        items.remove(item);
        return item;
    }
}
