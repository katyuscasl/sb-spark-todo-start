package ch.swissbytes.todo;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class ItemsNewIntegrationTest {

    static final ToDoListMain app = new ToDoListMain();

    @BeforeClass
    public static void setup() throws InterruptedException {
        app.startup(new EntityManager());
        Thread.sleep(700);//let's give it some time to start the server-socket, for slower machines
    }

    @AfterClass
    public static void tearDown() {
        app.shutdown();
    }

    @Test
    public void muestraPaginaConLosCamposDelItem() {
        HelperMethods.UrlResponse response =
                HelperMethods.doMethod("GET", "/items/new", null);

        assertThat(response, notNullValue());
        String body = response.body.trim();
        assertThat(body, notNullValue());
        assertThat(body.length(), greaterThan(0));
        assertThat(200, is(response.status));
        assertThat(body, containsString("<form "));
        assertThat(body, containsString("Title"));
        assertThat(body, containsString("id=\"title\""));
        assertThat(body, containsString("Description"));
        assertThat(body, containsString("id=\"description\""));
        assertThat(body, containsString("Save"));
        assertThat(body, containsString("Cancel"));
        assertThat(body, containsString("Back to list"));
    }

    @Test
    public void agregaUnNuevoItem() {
        HelperMethods.UrlResponse response =
                HelperMethods.doMethod("POST", "/items/new?title=un%20titulo&description=una%20descripcion", null);
        assertThat(response, notNullValue());
        String body = response.body.trim();
        assertThat(body, notNullValue());
        assertThat(body.length(), greaterThan(0));
        assertThat(200, is(response.status));
        assertThat(body, containsString("Item agregado exitosamente"));
    }

}
